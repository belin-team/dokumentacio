---
Title: "A rendszer telepítése"
Date: 2021. augusztus 28, 10:28:01 
Draft: false
Type: "root"
---
# A rendszer telepítése

Fontos! Mielőtt belekezdene a telepítésbe, mindenképpen készítsen biztonsági
mentést a fontos adatairól valamilyen külső eszközre (például CD, DVD,
pendrive, mobil merevlemez, másik számítógép).

A telepítés néhány egyszerű lépéssel elvégezhető, de a telepítéshez aktív
internetkapcsolat szükséges.

A telepítés megkezdéséhez nyomja meg az ENTER billentyűt az asztalon található
„BeLin 6.0 Telepítése” ikonon állva.

A megjelenő ablakban első lépésként a telepítés nyelvét lehet kiválasztani (a
magyar nyelv az alapértelmezett), ezután a Tab billentyű segítségével lépjen a
"Folytatás" gombra és nyomja meg az ENTER vagy a SZÓKÖZ billentyűt.

  
  

Ezt követően kiválaszthatja, milyen billentyűzetkiosztást szeretne használni
(a magyar az alapértelmezett), majd aktiválja a "Folytatás" gombot. A magyar
billentyűzetkiosztás az alapértelmezett beállítás.

  
  

A következő képernyőn megjelenő párbeszédablakban kiválaszthatja a telepítés
típusát az első fókuszba kerülő választógomb segítségével. A lehetséges
választási értékek: normál telepítés, minimális telepítés. A BeLin
felhasználóknak javasolt az alapértelmezetten kiválasztott normál telepítés
beállítás használata.

A párbeszédablakban található két jelölőnégyzet segítségével megadhatja, hogy
a telepítés közben letöltésre kerüljenek-e a biztonsági frissítések, és a
harmadik féltől származó kodekek és egyéb illesztőprogramok. Ha megadta az
Önnek megfelelő beállításokat, a TAB billentyű segítségével lépjen a Folytatás
gombra, majd aktiválja azt a SZÓKÖZ vagy az ENTER billentyűvel.

  
  

A Folytatás gomb aktiválása után a legkritikusabb rész, a particionálás
következik. Itt különösen oda kell figyelnie, hiszen egy rossz választás
könnyen adatvesztéshez vezethet. Amennyiben már van a számítógépén egy másik
operációs rendszer (például Microsoft Windows), és azt szeretné megtartani,
válassza az első alapértelmezetten kiválasztott lehetőséget. Az opció alatt
lévő csúszkával tudja átméretezni a kiválasztott partíciót (a Tab billentyűvel
találhatja meg), és így helyet felszabadítani a BeLin számára. Kérjük, vegye
figyelembe, hogy ezen a területen nem csak a Belin kap majd helyet, hanem az
Ön saját könyvtára is, amiben az adatait tárolhatja. Amennyiben egy NTFS
partíciót szeretne átméretezni a telepítés során, a biztonság kedvéért nem árt
azt töredezettség-mentesíteni a telepítés megkezdése előtt.

Bár a Windowsos partíciókat elérheti BeLin alól is, a gyakran módosuló
dokumentumokat, és a frissen elkészített, letöltött állományokat érdemes a
BeLin alatt létrehozott saját könyvtárában tárolni. Ha teheti, legalább 20
gigabájt helyet szánjon A BeLinnek, ezalatt már kényelmesen berendezkedhet.

Ha új számítógépre, merevlemezre telepít, vagy nem szeretné megőrizni a
számítógépen található adatokat, válassza a "Merevlemez törlése, és BeLin
telepítése" opciót, és (ha több merevlemeze van) válassza ki azt a meghajtót,
amire telepíteni szeretne.

Végül, ha a teljes particionálást szeretné kézben tartani, válassza a "Valami
más (kézi beállítás)" opciót. Ez a lehetőség haladó felhasználók számára a
lehető legnagyobb szabadságot és rugalmasságot kínálja. Kérjük, ha nem ért
hozzá, ne kezdjen bele, hiszen pillanatok alatt véletlenül is törölheti a
fontos adatait!

Ha végzett a particionálási beállításokkal, aktiválja a "Folytatás" gombot. A
folytatás gomb aktiválása után a fájlok másolása megkezdődik.

A fájlok másolása közben megjelenő párbeszédablakban válassza ki a régiót és a
várost az időzóna beállításához (a magyar időzóna az alapértelmezett), majd
aktiválja a "Folytatás" gombot.

  
  

Az időzóna beállítása után megjelenő párbeszédablakban adja meg a felhasználói
adatait. Az itt létrehozott felhasználó a későbbiekben - jelszavát megadva -
rendszergazdai jogosultságokat szerezhet, így ha több felhasználója is lesz a
gépnek, itt mindenképpen annak az adatait adja meg, aki később a rendszer
konfigurálását, karbantartását végzi majd. Ha ezzel végzett, aktiválja a
"Folytatás" gombot.

Ezt követően várnia kell a telepítés befejeződéséig.

Ha a telepítő végzett, rögtön újraindíthatja a gépet, vagy használhatja tovább
a BeLin 6.0 kiadást a DVD-ről. Ha az újraindítást választja, a BeLin 6.0
kiadás leáll, és a DVD lemez kiadásra kerül.

Vegye ki a lemezt, majd nyomja meg az ENTER billentyűt. Így hamarosan újra
elindul a BeLin 6.0 kiadás, ezúttal azonban már a merevlemezről.

## Telepítés utáni fontos tudnivaló

A számítógép elindulása után egy indítómenü jelenik meg, melynek
alapértelmezetten kiválasztott menüpontja a BeLin 6.0 elindítása. A korábbi
operációs rendszert a lefelé nyíl és az ENTER megnyomása után tudja
elindítani. Tipp: ha az indítómenüben lenyomja az END billentyűt, a korábbi
operációs rendszer kerül elindításra.

  
  

[Vissza a főoldalra](index.html)

