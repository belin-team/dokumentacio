---
Title: "A Mate asztali környezet alapvető billentyűparancsai"
Date: 2021. augusztus 28, 10:28:01 
Draft: false
Type: root
---
#A Mate asztali környezet alapvető billentyűparancsai
====================================================

Globális gyorsbillentyűk
------------------------

A globális gyorsbillentyűk lehetővé teszik a billentyűzet használatát az
asztalhoz kapcsolódó feladatok végrehajtásához, az aktuális ablakhoz
vagy alkalmazáshoz kapcsolódó feladatok helyett. Az alábbi táblázat
felsorol néhány globális gyorsbillentyűt:

  

<table>
<thead>
<tr class="header">
<th><p>Gyorsbillentyű</p></th>
<th><p>Funkció</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>STARTGOMB</p></td>
<td><p>Megnyitja az alkalmazások keresése szerkesztőmezőt.</p></td>
</tr>
<tr class="even">
<td><p>Alt+F1</p></td>
<td><p>Megnyitja az Alkalmazások menüt.</p></td>
</tr>
<tr class="odd">
<td><p>Alt+F2</p></td>
<td><p>Megnyitja azAlkalmazás futtatása párbeszédablakot.</p></td>
</tr>
<tr class="even">
<td><p>Print Screen</p></td>
<td><p>Egy képernyőképet készít a teljes asztalról.</p></td>
</tr>
<tr class="odd">
<td><p>Alt+Print Screen</p></td>
<td><p>Képernyőképet készít a fókuszban lévő ablakról.</p></td>
</tr>
<tr class="even">
<td><p>Ctrl+Alt+Balra nyíl, CTRL+ALT+Jobbra nyíl</p></td>
<td><p>Átvált a jelenlegi munkaterületről az előző, vagy a következő munkaterületre</p></td>
</tr>
<tr class="odd">
<td><p>STARTGOMB+D</p></td>
<td><p>Minimalizál minden ablakot és a fókuszt az asztalnak adja át.</p></td>
</tr>
<tr class="even">
<td><p>Alt+Tab, Alt+Shift+Tab</p></td>
<td><p>Váltás a megnyitott ablakok között</p></td>
</tr>
</tbody>
</table>

Az ablakok gyorsbillentyűi
--------------------------

<span id="gosbasic-58"></span>Az ablakok gyorsbillentyűi lehetővé teszik
a billentyűzet használatát feladatok végrehajtására a fókuszban lévő
ablakban. Az alábbi táblázat felsorol néhányat az ablak gyorsbillentyűi
közül:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Gyorsbillentyű</p></th>
<th><p>Funkció</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Alt+Tab, Alt+Shift+Tab</p></td>
<td><p>Váltás az ablakok között.</p></td>
</tr>
<tr class="even">
<td><p>Alt+F4</p></td>
<td><p>Bezárja a fókuszban lévő ablakot.</p></td>
</tr>
<tr class="odd">
<td><p>Alt+F5</p></td>
<td><p>Visszaállítja az aktuális ablak eredeti méretét, ha az maximalizált.</p></td>
</tr>
<tr class="even">
<td><p>Alt+F7</p></td>
<td><p>Áthelyezi a fókuszban lévő ablakot. A gyorsbillentyű megnyomása után áthelyezheti az ablakot az egér vagy a nyílbillentyűk használatával. Az áthelyezés befejezéséhez kattintson az egérrel vagy nyomjon meg egy billentyűt a billentyűzeten.</p></td>
</tr>
<tr class="odd">
<td><p>Alt+F8</p></td>
<td><p>Átméretezi a fókuszban lévő ablakot. A gyorsbillentyű megnyomása után átméretezheti az ablakot az egér vagy a nyílbillentyűk használatával. Az átméretezés befejezéséhez kattintson az egérrel vagy nyomjon meg egy billentyűt a billentyűzeten.</p></td>
</tr>
<tr class="even">
<td><p>Alt+F9</p></td>
<td><p>Az aktuális ablak minimalizálása.</p></td>
</tr>
<tr class="odd">
<td><p>Alt+F10</p></td>
<td><p>Az aktuális ablak maximalizálása.</p></td>
</tr>
<tr class="even">
<td><p>Alt+Szóköz</p></td>
<td><p>Megnyitja a kijelölt ablak ablakmenüjét. Az ablakmenü lehetővé teszi az ablak műveleteinek végrehajtását, mint a minimalizálás, munkaterületek közötti áthelyezés és a bezárás.</p></td>
</tr>
<tr class="odd">
<td><p>CTRL+SHIFT+Balra nyíl, CTRL+SHIFT+Jobbra nyíl</p></td>
<td><p>Áthelyezi az aktuális ablakot az előző, vagy a következő munkaterületre.</p></td>
</tr>
</tbody>
</table>

Alkalmazásbillentyűk
--------------------

<span id="gosbasic-59"></span><span id="gosbasic-61"></span>Az
alkalmazásbillentyűk lehetővé teszik az alkalmazás műveleteinek
végrehajtását. A gyorsbillentyűk használatával az alkalmazás egy adott
műveletét gyorsabban hajthatja végre, mint egér használata esetén. Az
alábbi táblázat felsorolja az általános alkalmazásgyorsbillentyűket:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Gyorsbillentyű</p></th>
<th><p>Művelet</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Ctrl+N</p></td>
<td><p>Egy új dokumentumot vagy ablakot készít.</p></td>
</tr>
<tr class="even">
<td><p>Ctrl+X</p></td>
<td><p>Kivágja a kijelölt szöveget vagy területet és elhelyezi a vágólapon.</p></td>
</tr>
<tr class="odd">
<td><p>Ctrl+C</p></td>
<td><p>Átmásolja a kijelölt szöveget vagy területet a vágólapra.</p></td>
</tr>
<tr class="even">
<td><p>Ctrl+V</p></td>
<td><p>Beilleszti a vágólap tartalmát.</p></td>
</tr>
<tr class="odd">
<td><p>Ctrl+Z</p></td>
<td><p>Visszavonja az utolsó műveletet.</p></td>
</tr>
<tr class="even">
<td><p>Ctrl+S</p></td>
<td><p>Lemezre menti az aktuális dokumentumot.</p></td>
</tr>
<tr class="odd">
<td><p>F1</p></td>
<td><p>Betölti az alkalmazás online súgó dokumentumát.</p></td>
</tr>
</tbody>
</table>

A fenti gyorsbillentyűkön kívül minden alkalmazás támogatja billentyűk
egy halmazát, amikkel a felhasználói felületen navigálhat és dolgozhat.
Ezek a billentyűk lehetővé teszik azon műveletek végrehajtását, amiket
általában egérrel végez. Az alábbi táblázat felsorol néhány
vezérlőbillentyűt:

<table>
<thead>
<tr class="header">
<th><p>Billentyűk</p></th>
<th><p>Művelet</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Tab, SHIFT+TAB</p></td>
<td><p>A párbeszédablakokban a TAB billentyűvel a következő, míg a SHIFT+TAB billentyűkombinációval az előző vezérlőelemre léphet.</p></td>
</tr>
<tr class="even">
<td><p>Enter vagy szóköz</p></td>
<td><p>Aktiválja vagy kiválasztja a kijelölt elemet.</p></td>
</tr>
<tr class="odd">
<td><p>F10</p></td>
<td><p>Aktiválja egy alkalmazás főmenüjét.</p></td>
</tr>
<tr class="even">
<td><p>Shift+F10</p></td>
<td><p>Aktiválja a kiválasztott elem helyi menüjét.</p></td>
</tr>
<tr class="odd">
<td><p>Esc</p></td>
<td><p>Elemkiválasztás nélkül zárja be a menüt vagy megszakítja a húzás műveletet.</p></td>
</tr>
</tbody>
</table>
[Vissza a kezdőlapra](index.html)
