---
Title: "Tudnivaló néhány akadálymentes alkalmazás használatáról"
Date: 2021. augusztus 28, 10:28:01 
Draft: false
---
# Tudnivaló néhány akadálymentes alkalmazás használatáról

## Webböngészés

### Firefox Webböngésző

Leírás: webböngésző

Honlap:

[http://www.mozilla.org](http://www.mozilla.org/)



Összegzés: folyamatos fejlesztés alatt áll, az Orka a legtöbb weblapot kezeli
ezzel a webböngészővel. A BeLin 6.0 Gnome és Mate kiadás már tartalmazza a
Firefox webböngészőt, külön letöltésre nincs szükség.



A következő táblázatban az Orka speciális billentyűparancsait tekintheti meg,
melyekkel könnyebben használhatja a Firefox webböngészőt:



Billentyűparancs

|

Művelet  
  
---|---  
  
h, Shift+h

|

Ugrás a következő vagy előző címsorra  
  
1, Shift+1

|

Ugrás a következő vagy előző 1. szintű címsorra (2-től 6-ig ugyanez történik,
csak a címsor szint változik)  
  
I, Shift+i

|

Ugrás a következő vagy előző listaelemre  
  
L, Shift+l

|

Ugrás a következő vagy előző listára  
  
O, Shift+o

|

Ugrás a következő vagy előző nagy objektumra  
  
Q, Shift+q

|

Ugrás a következő vagy előző idézet blokkra  
  
R, Shift+r

|

Ugrás a következő vagy előző élő régióra  
  
T, Shift+t

|

Ugrás a következő vagy előző táblázatra  
  
U, Shift+u

|

Ugrás a következő vagy előző nem meglátogatott hivatkozásra  
  
V, Shift+v

|

Ugrás a következő vagy előző meglátogatott hivatkozásra  
  
Shift+Alt+nyíl billentyűk

|

Ha egy táblázatban tartózkodik, a megfelelő irányba lép a táblázat cellái vagy
sorai között  
  
Shift+Alt+Home, Shift+Alt+End

|

Ha egy táblázatban tartózkodik, az első vagy az utolsó cellára ugrik a
táblázatban  
  
Bal Alt+le nyíl

|

Legördít egy kombinált listamezőt  
  
Ctrl+Home, Ctrl+End

|

A dokumentum elejére vagy végére ugrik  
  
Ctrl+balra vagy Ctrl+jobbra nyíl

|

A következő vagy előző szóra ugrik  
  
Orka módosító billentyű+balra vagy jobbra nyíl

|

A következő vagy előző objektumra ugrik  
  
Orka módosító billentyű+Tab vagy Orka módosító billentyű+Shift+Tab

|

A következő vagy előző űrlap mezőre lép  
  


Az Orka működését testre szabhatja, ha lenyomja az Orka módosító
billentyű+Ctrl+szóköz billentyűparancsot, majd a Firefox lapon megadja az
önnek megfelelő beállításokat.

Az Orka módosító billentyű asztali billentyűzet kiosztás esetén az Insert,
laptop billentyűzet kiosztás esetén a capslock billentyű.



## Azonnali üzenetküldés

### Pidgin

Leírás: Nagyon sok protokollt támogató azonnali üzenetküldő alkalmazás.

Parancs: pidgin

Weblap:

[http://gaim.sourceforge.net](http://gaim.sourceforge.net/)



Jól együtt működik az Orkával, a BeLin 6.0 kiadás tartalmazza.



#### A Pidgin első indítása

Amikor a programot először indítja el, egy beállító ablak jelenik meg. Ha
aktiválja a hozzáadás gombot és már van azonnali üzenetküldő fiókja, a
következőket kell tennie:

  1. Az első vezérlőelem a protokoll kombinált listamező. Nyomja meg a szóköz billentyűt, majd a nyíl billentyűk segítségével válassza ki azt a protokollt, melyet az ön azonnali üzenetküldő fiókja használ (például Google Talk, Irc). Ha kiválasztotta a kívánt protokollt, nyomja meg újra a szóköz billentyűt, ekkor a kombinált listamező bezáródik. 

  2. Töltse ki a szükséges mezőket a párbeszéd panel további részein, majd aktiválja a mentés gombot. 

  3. Ezután aktiválja a bezárás gombot, ha már nem akar további fiókokat beállítani. 



#### A csevegőablak és az Orka

Ha csevegni szeretne egy partnerrel, a partner listában válassza ki a nyíl
billentyűk segítségével, majd nyomja meg az Enter billentyűt. A csevegő ablak
két fő részre osztható:

  1. A csevegő területre, ahol az üzenetek megjelennek, 

  2. A szöveg területre, ahová a kimenő üzeneteket írhatja. 



Amikor cseveg egy partnerrel, az Orka a háttérben fut, és minden kimenő és
bejövő üzenetet felolvas. Használhatja az egyszerű áttekintés parancsokat a
felhasználói felület megtekintésére, ezek a következők:

7, 8, 9: felolvassa az előző, aktuális, következő sort

4, 5, 6: felolvassa az előző, aktuális, következő szót

1, 2, 3: felolvassa az előző, aktuális, következő karaktert



#### Módszerek a csevegés előzmények eléréséhez



Amikor cseveg egy partnerrel, az Orka mindig az utolsó üzenetet olvassa fel
automatikusan. Ha meg szeretné tekinteni az előző üzeneteket, számos lehetőség
áll a rendelkezésére:

  1. Használhatja az Orka módosító billentyű+F1-F9 billentyűparancsokat a megadott számú üzenet megtekintéséhez anélkül, hogy a szerkesztőmezőből kilépne. 

  2. Használhatja az egyszerű áttekintés parancsokat (lásd feljebb) 

  3. Az F6 billentyűparancs segítségével a csevegés előzményekre ugorhat, egy olvasható szerkesztőmezőben megtekintheti az előző üzeneteket. Ha elkezd gépelni, automatikusan a fókusz visszakerül a gépelés szerkeztőmezőbe. Ez a billentyűparancs például akkor lehet hasznos, ha valamit az előzményekből ki szeretne másolni a vágólapra. 



## Levelezés

### Mozilla Thunderbird

A BeLin 2.0 kiadástól az alapértelmezett levelezőszoftver a Mozilla
Thunderbird. Az Orkával jól használható ez a levelezőkliens. A
levelezőprogramot ugyanazokkal a billentyűparancsokkal ugyanúgy használhatja,
mint más operációs rendszereken. A Thunderbird képes az RSS hírcsatornák
kezelésére is. Számos akadálymentesítési beállítást előre elvégeztünk a
könnyebb használat érdekében (levelek megjelenítése létező üzenetablakban, a
beérkezett üzenetek mappa automatikus megjelenítése a kezdőoldal helyett,
stb).

  
  

## Szövegszerkesztés, táblázat kezelés, prezentációk készítése stb.

### Libreoffice

Leírás: A Libreoffice egy kiváló alternatívája a Microsoft Office irodai
programcsomagnak.  

A következő funkciók támogatottak:

Dokumentumok létrehozása és szerkesztése, helyesírás ellenőrzés, táblázatok
készítése.



### GEdit

Leírás: egyszerű szövegszerkesztő.

Honlap:

[http://www.gnome.org](http://www.gnome.org/)

Összegzés: jól együtt működik az Orkával.

### Pluma szövegszerkesztő

Leírás: egyszerű szövegszerkesztő, melyet a BeLin 6.0 kiadás Mate változata
alapértelmezetten tartalmaz. Ha ezt a szövegszerkesztőt a BeLin 6.0 GNOMe
változaton is szeretné használni, a pluma csomagot kell feltelepítenie.

Összegzés: jól együtt működik az Orkával, kényelmesen használható.

  
  

  
  

## Parancs sor/terminál

### GNOME Terminál

Leírás: parancs sor GNOME alatt.

Honlap:

[http://www.gnome.org](http://www.gnome.org/)

Parancs: gnome-terminal

Jól együtt működik az Orkával.

Néhány hasznos tipp és trükk a GNOME-terminal használatakor:

#### Kivágás, másolás, beillesztés

Ez könnyen elvégezhető, ha például egy Firefox által megnyitott weboldalról
kell beillesztenie valamit. Jelölje ki a kívánt szövegrészt, majd nyomja le a
CTRL+C billentyűkombinációt. A parancssorba a vágólapról a Ctrl+Shift+v
billentyűkombináció segítségével illesztheti be a vágólap tartalmát.

  
  

### Mate Terminál

Leírás: parancs sor, melyet a Mate asztali környezet alapértelmezetten
használ. A BeLin 6.0 kiadás Mate változata alapértelmezetten tartalmazza ezt
az alkalmazást.

Parancs: mate-terminal

Ez az alkalmazás jól együtt működik az Orkával.

Néhány hasznos tipp és trükk a Mate Terminál használatakor:

#### Kivágás, másolás, beillesztés

Ez könnyen elvégezhető, ha például egy Firefox által megnyitott weboldalról
kell beillesztenie valamit. Jelölje ki a kívánt szövegrészt, majd nyomja le a
CTRL+C billentyűkombinációt. A parancssorba a vágólapról a Ctrl+Shift+v
billentyűkombináció segítségével illesztheti be a vágólap tartalmát.

  
  

## Pdf fájlok kezelése

### Evince Dokumentummegjelenítő

A BeLin 4.0 kiadástól kezdve egy szöveges PDF fájlt gond nélkül elolvashat
ezzel az alkalmazással, ha a fájl megnyitása után engedélyezi a
mutatónavigáció használatát az F7 billentyűvel. Ha később újra megnyitja
ugyanazt a PDF fájlt amit olvasott, a mutatónavigációt már nem szükséges újra
engedélyeznie. Az Evince alkalmazás a GNOME asztali környezet része, így része
a BeLin 6.0 kiadásnak is. A BeLin 6.0 kiadás Mate változata szintén
alapértelmezetten tartalmazza ezt az alkalmazást.

### PDF fájlok konvertálása szövegfájllá parancssor használatával

Ha nem szeretné használni az Acrobat Reader alkalmazást a PDF fájlok
megjelenítésére, van arra lehetőség, hogy a PDF fájlokat szövegfájllá
konvertálja. Ehhez mindössze a következő parancsot kell futtatnia, majd a
Gedit szövegszerkesztővel megnyitva a szöveges fájlt elolvashatja a konvertált
PDF fájl tartalmát:

pdftotext pdffájlneve >txtfájlneve

Példa parancs:

pdftotext könyv.pdf >könyv.txt

Előfordulhat azonban, hogy olyan szerkezetű a PDF dokumentum, hogy ez a
konverzió nem hoz eredményt, az oldalak összekeverednek.

Megjegyzés: a Hangoskönyv Konverter alkalmazás is ugyanezzel a parancssoros
programmal állítja elő a PDF fájlok hangoskönyv változatát.

  
  

## Fájl letöltés

### Transmission

Leírás: GTK alapú torrent kliens.

Parancs: transmission

Weblap:

[_http://www.transmissionbt.com_](http://www.transmissionbt.com/)

Szinte teljesen akadálymentesen használható torrent kliens, az akadálymentes
kiadás tartalmazza.

Egyetlen hibája, hogy mindig az egyszerű áttekintés parancsok segítségével
kell megnéznie a torrent táblázatban a tartalmakat, de az alt+enter
billentyűkombináció segítségével megtekintheti a kijelölt elem tulajdonságait.

A Ctrl+o billentyűkombináció lenyomása után írja be a torrent fájl nevét amit
le szeretne tölteni és a letöltés megkezdődik. Az egyszerű áttekintés
billentyűparancsok segítségével információt kaphat a letöltés állapotáról.



## Hangszerkesztő alkalmazások

### Jokosher

Leírás: egyszerű hangszerkesztő.

Weblap:

[http://www.jokosher.org](http://www.jokosher.org/)

Összegzés: A 0.1.0 verzió óta akadálymentesen használható, de részletesen nem
lett tesztelve.

## Pénzügyek kezelése

### Gnu Cash

Leírás: Személyes pénzügyeit követő program.

Honlap:

[http://www.gnucash.org](http://www.gnucash.org/)

A 2.0 verzió óta akadálymentesen használható, de részletesen nem lett
tesztelve.

[Vissza a kezdőlapra](index.html)

