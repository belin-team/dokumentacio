---
Title: "Tilitoli (Sliding puzzle) dokumentáció"
Date: 2021. augusztus 28, 10:28:01 
Draft: false
Type: "root"
---
# Tilitoli (Sliding puzzle) dokumentáció

_Készítette: Ócsvári Áron_  

# 1\. Bevezetés

## 1.1. Tilitoli szabályai

A Tilitoli egy egyszemélyes logikai játék, amit 3x3, 4x4, vagy 5x5-ös méretű
táblán játszanak. A táblán a mérettől függően, 9-25 mező található. A mezőkön
egészszámok találhatóak, (1-8 1-15, 1-24) között, ezen felül egy üres mező. Az
üres mezővel felcserélhetőek a körülötte lévő mezők. A játék kezdetekor a
mezők elhelyezkedése véletlenszerű. A feladat az, hogy a számokat
sorfolytonosan rendezze úgy, hogy az üres mező a tábla jobb alsó sarkában
helyezkedjen el.

## 1.2. A játékról

Az alapvető szabályok itt is megmaradtak, vagyis csak az üres mező
mozgatásával lehet sorba rendezni a mezőket. A programban alapértelmezetten
háromféle méretű tábla található. Ezekhez egy-egy teszt pálya tartozik, a
többi a felhasználók által készült, amire ez a dokumentáció nem tér ki.

A 3X3 táblán az 1-8 közötti számok találhatóak, összekeverve. A kilencedik
mező az a bizonyos üres mező, aminek mozgatásával sorba kell rendezni a
mezőkön található számokat. A négyszer négyes és ötször ötös táblák sem sokban
különböznek az elsőtől, a négyszer négyesen 15, az ötször ötösön 24 számot
kell sorba rendezni.

Plusz kiegészítés, hogy a játékos is létrehozhat táblákat. Ehhez csak egy XML
fájlt kell készítenie, megadott szabályok szerint. Az egyes mezőkön nem csak
számok szerepelhetnek, hanem bármilyen szöveg/hang, amihez valamilyen
sorrendet lehet társítani. Jó alternatív példa erre Budapest kerületeinek
neve, ahol a sorrendet a kerület száma adja meg. De egy dallam is kirakható
sorrendet képez.

A tábla maximum ötször ötös lehet, ennél nagyobb játéktér már átláthatatlan
lenne. Ha az XML fájl kevesebb, mint 24 elemet tartalmaz, a táblára felkerül
megfelelő számú joker mező, ami a hiányzó elemek kiegészítésére szolgál. A
sorba rendezéskor csak arra kell figyelni, hogy az utolsó normál mező után a
jokerek következzenek és mindenképp a tábla utolsó eleme legyen az üres mező.
Könnyítés viszont, hogy ha több joker mező is található a táblán, azokat nem
kell sorrendbe állítani, a fő kritérium csak annyi, hogy egymást kövessék, és
a sorba rendezendő mezők után következzenek.

A mezők értékei hangfájlokban tárolódnak, így új típusú tábla létrehozásakor a
játékosnak minden értéket rögzítenie kell egy-egy hangfájlban. Ugyanígy a
játék üzenetei is egy-egy hangfájlban találhatóak, ezért nyitott a lehetőség a
Tilitoli más nyelvekre való lefordítására, vagy a háttérzene és egyéb hangok
megváltoztatására.

# 2\. A program használata

## 2.1. Rendszerkövetelmények

Az egyik talán legfontosabb kellék, a sztereo hangszóró, vagy fül/fejhallgató,
ez elengedhetetlen a játékhoz. Fontos még minimum 50 megabájtnyi memória, és
minimum 1 ghz processzor.

Windows XP használatakor a játék csak akkor indul el, ha fel van telepítve a
[Microsoft Visual C++ 2008 Redistributable
Package](http://www.microsoft.com/download/en/details.aspx?displaylang=en&id=29).

## 2.2. Telepítés

### 2.2.1. Windows

A telepítő üzeneteit végigolvasva feltelepíthető a játék. Kiválasztható a
parancsikonok létrehozása az asztalon, megváltoztatható a program főkönyvtára
is. A telepítés után a parancsikonok segítségével elindítható a játék.

### 2.2.2. Linux

A játékhoz elengedhetetlen a Python és a Pygame ugyanezen verzióinak megléte,
a telepítést a terminálban a sudo apt-get install python, és sudo apt-get
install python-pygame parancsokkal teheti meg.

A letöltött zip fájlt az unzip tilitoli0.x.x.zip paranccsal tömörítheti ki (a
.x.x az aktuális verziószámot jelzi).

Miután belépett a játék mappájába (cd mappanév), a python tilitoli.py parancs
kiadásával indíthatja el a játékot.

## 2.3. Főmenü

A játék indulásakor egy intró köszönti a játékost, amin bármely billentyű
lenyomásával továbbugorhat. Ekkor felhangzik a főmenü zenéje, és az első
menüpont neve (Új játék). A menüben a fel és le nyilakkal mozoghat, ami ugyan
a képernyőn nem látszik, de a virtuális kurzor mozog. Egy menüpontot
enterrel nyithat meg. A főmenüben öt menüpont található:

  * Új játék: Ha erre egy entert nyom, a pályaválasztó menübe jut (erről a következő fejezetben írok). 
  * Hangszóró teszt: A játékban fontos, hogy tudjuk, merről szól a bal, és a jobb hangszóró. Ez főleg fülhallgatónál lehet fontos, ahol könnyen összecserélhető a két oldal. 
  * Nyelvválasztás: Itt a nyelvet állíthatja át másikra, ami a játék bezárásáig vagy az újbóli megváltoztatásig érvényben marad. 
  * Az elmentett játék visszatöltése: Ha elmentett egy állást, a menüpont aktiválásával visszatöltheti. 
  * Kilépés: A menüpont aktiválásakor lehalkul a zene, és bezárul a program. 

## 2.4. Pályaválasztó menü

Az Új játék menüpont aktiválásával az elérhető pályák listája jelenik meg.
Itt is a fel és le nyíllal mozoghat, az enterrel indíthatja el a játékot.
Ekkor a menü zenéje elhalkul, és betöltődik a pálya.

## 2.5. A nyelvválasztó menü

A menüpont aktiválásakor megjelenik az elérhető nyelvek listája. Itt a fel és
le nyilakkal mozoghat, egy nyelvet az enter billentyűvel aktiválhat. Ekkor
visszatér a főmenübe, és a játék az adott nyelven fog megszólalni. A beállítás
a játék bezárásáig vagy annak megváltoztatásáig érvényes, az újbóli indításkor
az operációs rendszer alapértelmezett nyelvén szólal meg.

## 2.6. A pályákról

A táblán való mozgás a kurzormozgató billentyűkkel lehetséges. A fel és le
nyíllal soronként, a bal és jobbra nyíllal oszloponként mozog a virtuális
kurzor. Igyekeztem ezt hanghatásokkal is szimulálni, így ha jobbra-balra
mozog, az egyes oldalon a hangszórók hangereje változik, vagyis pl. teljesen
baloldalon csak a bal hangszóróból jön a hang. Az üres mezőt egy élesebb
hanggal jelölöm, így egyértelműen meg lehet különböztetni a többitől. Ha az
üres mező valamelyik szomszédján áll a virtuális kurzor, az enter
lenyomására helyet cserél az üres mezővel. A sikeres cserét egy jól
megkülönböztethető hang jelzi, ezzel segítve a felhasználót. Ha a tábla szélén
mozog, egy élesebb, csappanó hangot hall. Ez a háromszor hármas táblánál
zavaró lehet, de ötször ötösnél nagyon hasznos.

Ha sikeresen kirakta a jó sorrendet, a végén megtudhatja hány lépésből
sikerült megcsinálni. A lépések számát meghallgathatja az s billentyű
lenyomásával is.

Ha még nem végzett a játékkal, de szeretné máskor folytatni, a v billentyű
lenyomásával elmentheti azt. Figyelem, egyszerre csak egy tábla állásait
tárolja a program!

# 3\. XML fájl elkészítése

Az XML fájlok minden nyelv mappájában (pl. locale\hu, locale\en, stb.) az xmls
mappában találhatók. Minden fájlhoz hozzátartozik egy ugyanolyan nevű .ogg
fájl is, ami a pálya nevét tartalmazza. Ha valamelyik fájl nem található, a
pálya nem kerül bele a pályák listájába.

Jelenleg a program az XML fájlból csak az ID és a Sound mezőket használja. A
sound értéke a pálya könyvtára/a mező neve, pl. a háromszor hármasnál
<sound>3x3/3.ogg</sound>. Fontos, hogy a Python a \ helyett /-t vár könyvtár
elválasztó jelként.

# 4\. Billentyűparancsok a játékban

Az átláthatóság kedvéért, itt megtalálható az összes billentyűparancs, ami
használható a játékban.

Billentyűparancs | leírás  
---|---  
Home | A menükben a legfelső menüpontra ugrik, a táblán a bal szélső oszlopra.  
end | A menükben a legutolsó elemre ugrik, a táblán a jobb szélső oszlopra.  
enter | A menükben aktiválja a kijelölt menüpontot, a játékban az üres és az
adott mezőt kicseréli, ha lehetséges.  
Fel nyíl | Felfelé mozog a menüben és a játéktáblán.  
Le nyíl | Lefelé mozog a menüben és a játéktáblán.  
Balra nyíl | A játéktáblán az oszlopokon lépked bal irányban.  
Jobbra nyíl | A játéktáblán az oszlopokon lépked jobb irányban.  
Page up | A játéktáblán a legfelső sorra ugrik.  
Page down | A játéktáblán a legalsó sorra ugrik  
r | Elhangzik a tábla azon sorának száma, ahol a virtuális kurzor áll.  
c | Elhangzik a tábla azon oszlopának száma, ahol a virtuális kurzor áll.  
e | Megismétli a virtuális kurzornál lévő mező értékét.  
s | Bemondja az eddigi cserék számát.  
o | Végigmondja az elemeket, a helyes sorrendben.  
v | Elmenti a pálya aktuális állását.  
f2 | Megállítja a háttérzene lejátszását.  
F3 | Folytatja a háttérzene lejátszását.  
Escape | Bezárja a pályát, a főmenüben megnyomva kilép a játékból.  
  
# 5\. Továbbfejlesztési lehetőségek

Rengeteg ötletem van még, amivel fejleszthetném a játékot, viszont
tanulmányaimból kifolyólag ezek eddig még nem valósultak meg. Felmerült a
játékosok versenyeztetése, vagyis a program egy sikeres játék után elküldené
egy szervernek az eredményeket (hány lépésből rakta ki a felhasználó, mennyi
idő alatt, stb), amit egy program kiértékelne.

Az 5.1-es hangzás ötlete is felvetődött, így a nagyobb pályákon való mozgáskor
még jobb hanghatásokat lehetne vele elérni. Ezt már előkészíti a hang lejátszó
függvény, így a közeljövőben szeretném ezt megvalósítani.

A könnyebb pályaszerkesztés segítésére tervben van egy xml szerkesztő
beleépítése a játékba, így akik nem ismerik ezt a nyelvet, ők is nagyon
egyszerűen tudnának pályákat készíteni.

Mivel a felhasználókra van bízva a pályák elkészítése, szeretnék egy
interaktív weboldalt is csinálni, ahova feltölthetik a játékosok a saját
készítésű pályákat, így másoknak nem kellene feleslegesen dolgozniuk.

# 6\. Elérhetőségek

A Tilitoli legfrissebb verzióját [innen töltheti
le.](http://audiopuzzle.googlecode.com)

E-mail: [oaron1@gmail.com](mailto:oaron1@gmail.com)

