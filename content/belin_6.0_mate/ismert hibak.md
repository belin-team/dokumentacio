---
Title: "Ismert hibák BeLin 6.0 Mate kiadás használata esetén"
Date: 2021. augusztus 28, 10:28:01 
Draft: false
Type: "root"
---
# Ismert hibák BeLin 6.0 Mate kiadás használata esetén

  * Az Indikátor menüt nem lehet elérni a BeLin 6.0 Mate alapú kiadásánál a STARTGOMB+S billentyűkombinációval. Az indikátor menühöz úgy lehet hozzáférni, hogy a CTRL+ALT+ESCAPE billentyűkombinációval át kell lépni az alsó panelre, a TAB billentyűvel meg kell keresni az indikátor kisalkalmazást, majd a JOBBRA vagy a BALRA NYÍL segítségével le kell gördíteni az indikátor menüt.

  

[Vissza a kezdőlapra](index.html)

