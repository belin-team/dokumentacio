---
Title: ""
Date: 2021. augusztus 28, 10:28:01 
Draft: false
Type: "root"
---
# BeLin 6.0 kiadás - kezdőlap

Üdvözöljük a BeLin 6.0 kiadás dokumentációs oldalán!  
  
  

## Miért készült a BeLin és miért pont ez a neve?

A cél az volt, hogy mintegy alternatív lehetőségként a látássérült
felhasználók ki tudják használni a Linux operációs rendszer nyújtotta
lehetőségeket.

Az Ubuntu név a Canonical bejegyzett védjegye, így a kiadásnak más nevet
kellett választani. A BeLin név a Beszélő Linux név rövidítése.

A rendszer számos akadálymentesen használható alkalmazást tartalmaz. Néhány
ezek közül a teljesség igénye nélkül:  
A Firefox webböngészővel kényelmesen internetezhet, az ingyenes Libreoffice
irodai programcsomag segítségével Microsoft Office dokumentumokat,
táblázatokat szerkeszthet és jeleníthet meg, a Pidgin üzenetküldő
alkalmazással számos azonnali üzenetküldő protokoll használatával cseveghet
másokkal, a Thunderbird levelezőprogram segítségével elektronikus leveleket
küldhet és fogadhat, RSS hírcsatornákat böngészhet.

## Melyik Ubuntu verzióra épül a BeLin 6.0 kiadás?

A BeLin 6.0 kiadás akadálymentesítési okokból az Ubuntu 18.04.1 (Bionic
Beaver) verziójára épül. Mielőtt használatba venné az operációs rendszert,
érdemes elolvasnia a következő dokumentumot:

  * [A BeLin 6.0 kiadás újdonságai](ujdonsagok.html)

## A Beszélő Linux letöltése

A BeLin 6.0 kiadás mind a GNOME, mind a Mate asztali környezetet használó
változatokban is elérhető.

Ha Önnek képernyő-nagyító funkcióra van szüksége, a GNOME asztali környezetre
épülő BeLin 6.0 kiadás használatát javasoljuk.

Figyelem!

A GNOME asztali környezetre épülő BeLin 6.0 kiadás csak 64 bites architektúrát
támogató processzorokhoz érhető el.

  
  

Ha a számítógépében 2 GB, vagy annál kevesebb memória áll rendelkezésre, a
Mate alapú BeLin 6.0 kiadás használatát javasoljuk.

Ha olyan processzorral rendelkezik, amelyik támogatja a 64 bites
architektúrát, akkor a 64 bites DVD képfájlt töltse le, egyéb esetben a 32
bites képfájlt ajánljuk. Csak 32 bites architektúrát támogatnak például az
Intel Celeron, Athlon XP processzorok. Ha nem biztos benne, hogy az ön
számítógépe támogatja-e a 64 bites architektúrát, inkább a 32 bites változatot
használja.

Ha a számítógépe már a modern UEFI szabbványt támogatja, mindenképpen a 64
bites változatot kell letöltenie.

A BeLin kiírásához DVD lemezre lesz szüksége, a letöltött DVD képfájlt például
felírhatja a lemezre az ingyenesen használható Imgburn segédprogrammal.

    
    
    [A BeLin 6.0 32 bites Mate alapú kiadás letöltése](http://belin.hu/letoltes/belin-6.0-mate-i386.iso)
    [A BeLin 6.0 64 bites GNOME alapú kiadás letöltése](http://letoltes/belin-6.0-gnome-amd64.iso)
    [A BeLin 6.0 64 bites Mate alapú kiadás letöltése](http://letoltes/belin-6.0-mate-amd64.iso)

Md5sum ellenőrző összegek a letöltés épségének ellenőrzéséhez

Nagy fájlok letöltése után érdemes ellenőrizni épségüket. Erre elterjedt
módszer egy hash-függvény értékének ellenőrzése. Az MD5 egy olyan hash-
függvény, amely tetszőleges méretű fájlból egy 128 bites kivonatot készít,
amely a fájl egyetlen bithibája esetén is jelentősen eltérő értéket vesz fel.

    
    
    [A BeLin 6.0 32 bites Mate alapú kiadás ellenőrző összegének letöltése](http://belin.hu/letoltes/belin-6.0-mate-i386.md5)
    [A BeLin 6.0 64 bites GNOME alapú kiadás ellenőrző összegének letöltése](http://belin.hu/letoltes/belin-6.0-gnome-amd64.md5)
    [A BeLin 6.0 64 bites Mate alapú kiadás ellenőrző összegének letöltése](http://belin.hu/letoltes/belin-6.0-mate-amd64.md5)

##  A korábbi BeLin 5.01 kiadás letöltése

    
    
    [A BeLin 5.01 32 bites GNOME alapú kiadás letöltése](http://belin.hu/letoltes/belin-5.01-gnome-i386.iso)
    [A BeLin 5.01 32 bites Mate alapú kiadás letöltése](http://letoltes/belin-5.01-mate-i386.iso)
    [A BeLin 5.01 64 bites GNOME alapú kiadás letöltése](http://letoltes/belin-5.01-gnome-amd64.iso)
    [A BeLin 5.01 64 bites Mate alapú kiadás letöltése](http://letoltes/belin-5.01-mate-amd64.iso)

##  A BeLin 5.0 Webböngészés és levelezés tananyag letöltése

[BeLin 5.0 webböngészés és levelezés tananyag (HTML változat, zippel
tömörítve)](http://belin.hu/letoltes/BeLin_5.0_tananyag/BeLin_5.0_Webbongeszes_es_levelezes_tananyag_html.zip)

[BeLin 5.0 webböngészés és levelezés tananyag (nagybetűs PDF
változat)](http://belin.hu/letoltes/BeLin_5.0_tananyag/BeLin_5.0_Webbongeszes_es_levelezes_tananyag_Calibri18.pdf)

[BeLin 5.0 webböngészés és levelezés tananyag (DAISY
változat)](http://belin.hu/letoltes/BeLin_5.0_tananyag/BeLin_5.0_Webbongeszes_es_levelezes_tananyag_Daisy.zip)

[BeLin 5.0 webböngészés és levelezés tananyag (mp3
változat)](http://belin.hu/letoltes/BeLin_5.0_tananyag/BeLin_5.0_Webbongeszes_es_levelezes_tananyag_MP3.zip)

## A BeLin 6.0 kiadás dokumentációjának olvasása

Ha már letöltötte az Önnek legjobban megfelelő BeLin 6.0 kiadásváltozat
lemezképét, ebben a szekcióban tudja kiválasztani a megfelelő BeLin 6.0
kiadásváltozat dokumentációját a további olvasáshoz.

  * [A BeLin 6.0 Gnome alapú kiadás dokumentációjának megjelenítése](belin_6.0_gnome/index.html)

  * [A BeLin 6.0 Mate alapú kiadás dokumentációjának megjelenítése](belin_6.0_mate/index.html)

## A Korábbi, még támogatott BeLin kiadások dokumentációinak megjelenítése

A korábbi BeLin kiadások közül a BeLin 5.0 alapú kiadás támogatott. A BeLin
5.0 alapú kiadás 2021. áprilisáig kap biztonsági frissítéseket.

  * [A BeLin 5.01 Gnome alapú kiadás dokumentációjának megjelenítése](belin_5.0_gnome/index.html)

  * [A BeLin 5.01 Mate alapú kiadás dokumentációjának megjelenítése](belin_5.0_mate/index.html)

## Hol kaphatok segítséget?

Ezen dokumentációs oldal mellett akadálymentesítési problémákkal kapcsolatban
mindenki előtt nyitott a BeLin levelezési lista, melyre [itt tud feliratkozni](http://linux.infoalap.eu/mailman/listinfo/linux_linux.infoalap.eu).

A listaszabályzattal kapcsolatos további tudnivalókért nézze meg a [Gyakran
ismételt kérdések](gyik/gyik.html) című dokumentumot.

Az Infoalap Braille-szerkesztő elkészítését támogatta az Invitech Solutions és
a Magyar Vakok és Gyengénlátók Országos Szövetsége.

